import requests
import json
import os
import hashlib
from requests.auth import HTTPBasicAuth

hostname = "http://localhost:5000"
# user = 'rfdickerson'
# password = 'awesome'
rest_prefix="/v1"


''' Important functions
create a game
leave a game
update game state with location
cast a vote
'''

def create_user(username, password, firstname, lastname):

	payload = {'username': username, 'password': password, 'firstname': firstname, 'lastname': lastname}
	url = "{}{}{}".format(hostname, rest_prefix, "/register")
	r = requests.post(url, data=payload)

	response = r.json()
	#print r
	print response["status"]

	print response["reason"]

def create_game(username, password, gamename, description):
	payload = {'gamename': gamename, 'description': description}
	url = "{}{}{}".format(hostname, rest_prefix, "/game")
	print 'sending {} to {}'.format(payload, url)
	r = requests.post(url, auth=(username, password), data=payload)

	response = r.json()

	print response
	join_game(username,password, response['gameID'])

	
	return response['gameID']


def join_game(username, password, game_id):
	print 'Joining game id {}'.format(game_id)
	payload = {'game_id': game_id}
	url = "{}{}/game/{}/lobby".format(hostname, rest_prefix, game_id)
	r = requests.post(url, auth=(username, password))
	r = r.json()
	print r


def delete_game(username,password, game_id):

	print 'Deleting game id {}'.format(game_id)
	payload = {'game_id': game_id}
	url = "{}{}/game/{}".format(hostname, rest_prefix, game_id)
	r = requests.delete(url, auth=(username, password))
	r = r.json()
	print r

def update_location(username, password, game_id, lat, lng):
	""" reports to the game your current location, and the game 
	returns to you a list of players nearby """

	payload = {'lat': lat, 'lng': lng}
	url = "{}{}/game/{}".format(hostname, rest_prefix, game_id)
	r = requests.put(url, auth=(username, password), data=payload)
	response = r.json()

	print response
##########################################################################################################

def game_info(game_id, username, password):
	## returns all the players, the time of day, and other options for the game 
	r = requests.get(hostname + rest_prefix + "/game/" + str(game_id), auth=(username, password))
	response = r.json()
	print response

def cast_vote(username, password, game_id, target_username):
	payload = {'target_username': target_username}
	
	r = requests.post(hostname + rest_prefix + "/game/" + str(game_id) + "/ballot", data=(payload), auth=(username,password))
	response = r.json()

	print response

def count_vote(username, password, game_id):

	
	r = requests.get(hostname + rest_prefix + "/game/" + str(game_id) + "/ballot",  auth=(username,password))
	response = r.json()

	print response

def set_game_state(game_state):
	payload = {'game_id': game_id, 'game_state': 'night'}
	r = requests.post(hostname + rest_prefix + "/game/admin")
	r = r.json()
	print response


def get_games(username, password):
	r = requests.get(hostname + rest_prefix + "/game")
	r = r.json()
	return r["results"]


def create_users():
	create_user('michael', 'paper', 'Michael', 'Scott')
	create_user('dwight', 'paper', 'Dwight', 'Schrute')
	create_user('jim', 'paper', 'Jim', 'Halpert')
	create_user('pam', 'paper', 'Pam', 'Beesly')
	create_user('ryan', 'paper', 'Ryan', 'Howard')
	create_user('andy', 'paper', 'Andy', 'Bernard')
	create_user('angela', 'paper', 'Angela', 'Martin')
	create_user('toby', 'paper', 'Toby', 'Flenderson')

def werewolf_winning_game():
	game_id = create_game('michael', 'paper', 'NightHunt', 'A test for werewolf winning')
	games = get_games('michael', 'paper')
	for game in games:
		print "Id: {},\tName: {}".format(game["game_id"], game["name"])
	
	join_game('dwight', 'paper', game_id)
	join_game('jim', 'paper', game_id)
	join_game('pam', 'paper', game_id)
	join_game('ryan', 'paper', game_id)
	join_game('andy', 'paper', game_id)
	join_game('angela', 'paper', game_id)
	join_game('toby', 'paper', game_id)
	start_game('michael', 'paper', game_id)
	
	leave_game('micheal', 'paper', game_id)


def create_users():
	create_user('michael', 'paper01', 'Michael', 'Scott')
	create_user('dwight', 'paper02', 'Dwight', 'Schrute')
	create_user('jim', 'paper03', 'Jim', 'Halpert')
	create_user('pam', 'paper04', 'Pam', 'Beesly')
	create_user('ryan', 'paper05', 'Ryan', 'Howard')
	create_user('andy', 'paper06', 'Andy', 'Bernard')
	create_user('angela', 'paper07', 'Angela', 'Martin')
	create_user('toby', 'paper08', 'Toby', 'Flenderson')

def all_join_game(current_game_id):
	join_game('dwight', 'paper02', current_game_id)
	join_game('jim', 'paper03', current_game_id)
	join_game('pam', 'paper04', current_game_id)
	join_game('ryan', 'paper05', current_game_id)
	join_game('andy', 'paper06', current_game_id)
	join_game('angela', 'paper07', current_game_id)
	join_game('toby', 'paper08', current_game_id)

	username_password_playerid_list = []  # hard coded this for voting, no better way for our case
	username_password_playerid_list.append({'username': 'michael', 'password': 'paper01', 'playerid': 1})
	username_password_playerid_list.append({'username': 'dwight', 'password': 'paper02', 'playerid': 2})
	username_password_playerid_list.append({'username': 'jim', 'password': 'paper03', 'playerid': 3})
	username_password_playerid_list.append({'username': 'pam', 'password': 'paper04', 'playerid': 4})
	username_password_playerid_list.append({'username': 'ryan', 'password': 'paper05', 'playerid': 5})
	username_password_playerid_list.append({'username': 'andy', 'password': 'paper06', 'playerid': 6})
	username_password_playerid_list.append({'username': 'angela', 'password': 'paper07', 'playerid': 7})
	username_password_playerid_list.append({'username': 'toby', 'password': 'paper08', 'playerid': 8})
	return username_password_playerid_list

def clean_game_data():
	r = requests.delete(hostname + rest_prefix +"/reset" )
	response = r.json()
	return response   
if __name__ == "__main__":

	#create_users()
	#create_user('Herb11', 'corndogs', 'Herb', 'Hill')
	#create_user('michael', 'paper20', 'Michael', 'X')
	clean_game_data()

	create_users()
	

	current_gameid=create_game('michael', 'paper01', 'NightHunt', 'A game in Austin')


	all_join_game(current_gameid)

	update_location('dwight', 'paper02', current_gameid, 1, 5)

	game_info(current_gameid, 'michael','paper01')

	cast_vote('michael', 'paper01', current_gameid, "jim")
	cast_vote('pam', 'paper04', current_gameid, 'jim')

	count_vote('michael','paper01', current_gameid)

	#delete_game('michael', 'paper01', current_gameid)
   # update_game('rfdickerson', 'awesome', 80, 20)
   # game_info('rfdickerson', 'awesome', 22)
   # leave_game('rfdickerson', 'awesome', 302)
