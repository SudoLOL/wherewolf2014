from flask import Flask, request, jsonify
import psycopg2
from wherewolfdao import WherewolfDao
import hashlib
import random
import string
app = Flask(__name__)

def get_db(databasename='wherewolf', 
        username='tom',
        password='password'):

        
    return psycopg2.connect(database= databasename,
             user=username, password=password)

@app.route("/v1")
def hello():
    return "Hello World!"

def check_password(username, password):
    print 'checking password for {}'.format(username)
    conn = get_db()
    
    sql = ('select password, salt from gameuser '
           'where username=%s')
    cur = conn.cursor()
    cur.execute(sql, (username, ))
    hash1, salt = cur.fetchall()[0]

    #print '----------------{}'.format(dbpass)
    hashedpass= hashlib.sha256(salt+password).hexdigest()

    if hash1 == hashedpass:
        return True
    else:
        return False
    
@app.route('/v1/register', methods=["POST"])
def create_user():
    dao=WherewolfDao('wherewolf','tom','password')
    username = request.form['username']
    password = request.form['password']
    firstname = request.form['firstname']
    lastname = request.form['lastname']
    rng=random.SystemRandom()
    salt=''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(10))
    print salt
    hashedpass=hashlib.sha256(salt+password).hexdigest()

    response = {}
    response["status"] = "failure"
    
    if len(password)<6:
        response["reason"] = "password too short"
    
    else:
        try:
            dao.create_user(username,hashedpass,firstname,\
                lastname,salt)
            response["status"] = "success"
            response['reason']="success"
        except:

            response["status"]="failure"
            response["reason"] = "user already exists"
        response["username"] = username

    return jsonify(response)

@app.route('/v1/game', methods=["POST"])
def create_game():
    username = request.authorization['username']
    password = request.authorization['password']
    gamename = request.form['gamename']
    description = request.form['description']
    response={}

    dao=WherewolfDao('wherewolf','tom','password')

    if check_password(username,password) == True:
    	
    	if dao.get_user_games(username)== None:

        	createdgameID=dao.create_game(username, gamename, description)
        	response['status']= 'sucess'
        	response['gameID']= createdgameID
        else:
        	response["status"] = "failure"
        	response["reason"] = "user already administering a game"
    else:
        response["status"]="failure"
        response["reason"] = "user authorization invalid"
    return jsonify(response)


@app.route('/v1/game/<game_id>/lobby', methods=["POST"])
def join_game(game_id):
	username = request.authorization['username']
	password = request.authorization['password']

	if check_password(username, password) == True:

		#check status of game (0=lobby)

		dao=WherewolfDao('wherewolf','tom','password')
		games=dao.get_games()
		response ={}

		isavailable=False
		for game in games:
			if game['game_id'] == int(game_id) and game['status'] == int(0):
				isavailable = True

		#check that user is not in another game
		useravailable=True
		joinedgames=dao.get_user_games(username)

		if joinedgames != None:
			useravailable=False

		#add user to the game

		if useravailable==True and isavailable==True:
			dao.join_game(username, game_id, 0, 0)
			response['status']= 'success'
		elif useravailable ==False:
			response["status"]="failure"
			response["reason"] = "user already in another game"
		else:
			response["status"] ="failure"
			response["reason"] = "the game cannot be joined: it may have already started or ended"    

	else:
		response["status"] ="failure"
		response["reason"] = "invalid authorization"    
	return jsonify(response)

@app.route('/v1/game/<int:game_id>', methods=["DELETE"])
def delete_game(game_id):
	username = request.authorization['username']
	password = request.authorization['password']
	response ={}

	if check_password(username, password) == True:

		#check if user is admin

		dao=WherewolfDao('wherewolf','tom','password')
		user_id= dao.get_userid(username)
		response['user_id']=user_id

		games=dao.game_info(game_id)
		if games['admin_id']==user_id:
			dao.delete_game(game_id)
			response['status']= 'success'
			
		else:
			response["status"] ="failure"
			response["reason"] = "you are not the admin of this game"   			
	else:
		response["status"] ="failure"
		response["reason"] = "invalid authorization"    
	return jsonify(response)

@app.route('/v1/game/<game_id>', methods=["PUT"])
def update_location(game_id):
	username = request.authorization['username']
	password = request.authorization['password']
	lat = request.form['lat']
	lng = request.form['lng']
	response ={}

	if check_password(username, password) == True:		
		dao=WherewolfDao('wherewolf','tom','password')
		dao.set_location(username, lat, lng)
		response['status'] = 'success'	
		response['wherewolfscent']= dao.get_alive_nearby(username, game_id, 50)
	else:
		response["status"] ="failure"
		response["reason"] = "invalid authorization"    
	return jsonify(response)
@app.route('/v1/game/<game_id>', methods=["GET"])
def getinfo(game_id):
	username = request.authorization['username']
	password = request.authorization['password']

	response ={}

	dao=WherewolfDao('wherewolf','tom','password')
	if check_password(username, password) == True:	
		response = dao.game_info(game_id)
	else:
		response["status"] ="failure"
		response["reason"] = "invalid authorization"   

	 
	return jsonify(response)

@app.route('/v1/game/<game_id>/ballot', methods=["POST"])
def vote(game_id):
	username = request.authorization['username']
	password = request.authorization['password']
	target_username = request.form['target_username']

	#get users's playername 

	response ={}

	dao=WherewolfDao('wherewolf','tom','password')

	player_id=dao.get_playerid(username)
	target_id=dao.get_playerid(target_username)
	

	if check_password(username, password) == True:	
		dao.vote(game_id, player_id, target_id)
		response['status'] = 'success'
	else:
		response["status"] ="failure"
		response["reason"] = "invalid authorization"   

	 
	return jsonify(response)

@app.route('/v1/game/<game_id>/ballot', methods=["GET"])
def count_vote(game_id):
	username = request.authorization['username']
	password = request.authorization['password']


	#get users's playername 

	response ={}

	dao=WherewolfDao('wherewolf','tom','password')
	

	if check_password(username, password) == True:	

		votes=[]
		for i in dao.count_vote(game_id):
			entry={}
			entry['playerid']=i[0]
			entry['votes']=i[1]
			votes.append(entry)

		response['votes'] =votes
		response['status'] = 'success'
	else:

		response["status"] ="failure"
		response["reason"] = "invalid authorization"   

	 
	return jsonify(response)
@app.route('/v1/reset', methods=["DELETE"])
def reset():
	
	response ={}
	dao=WherewolfDao('wherewolf','tom','password')
	dao.clear_tables()

	 
	return jsonify(response)

if __name__ == "__main__":
    app.run(debug=True)